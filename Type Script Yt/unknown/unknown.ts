let userInput: unknown;    //you can enter any type of value in unknown
userInput = 5;
console.log(userInput)
console.log(typeof(userInput)) //number
userInput = "some value";
console.log(userInput)
console.log(typeof(userInput))  //string

