const person: {
  name: string; //name must store string value
  age: number; //age must store number value
} = {
  name: "sun",
  age: 30,
};

console.log(person);
