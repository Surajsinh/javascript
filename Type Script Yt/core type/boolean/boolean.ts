let add = (n1:number,n2:number,show:boolean) => {
    
    if(show){
        console.log(n1+n2);
    }
    else{
        return n1+n2;
    }
}
const num1 = 5;
const num2 = 6.5;
const showResult = true;

add(num1,num2,showResult);