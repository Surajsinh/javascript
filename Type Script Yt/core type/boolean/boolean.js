var add = function (n1, n2, show) {
    if (show) {
        console.log(n1 + n2);
    }
    else {
        return n1 + n2;
    }
};
var num1 = 5;
var num2 = 6.5;
var showResult = true;
add(num1, num2, showResult);
