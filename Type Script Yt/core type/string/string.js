var add = function (n1, n2, show, phrase) {
    var res = n1 + n2;
    if (show) {
        console.log(phrase + res);
    }
    else {
        return n1 + n2;
    }
};
var num1 = 5;
var num2 = 6.5;
var showResult = true;
var str = "result = ";
add(num1, num2, showResult, str);
