let add = (n1: number, n2: number, show: boolean, phrase: string) => {
  const res = n1 + n2;
  if (show) {
    console.log(phrase + res);
  } else {
    return n1 + n2;
  }
};
const num1 = 5;
const num2 = 6.5;
const showResult = true;
const str = "result = ";

add(num1, num2, showResult, str);
