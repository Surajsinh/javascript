//import {addProject} from './addproject.js';

const title = document.querySelector("#title") as HTMLSelectElement;
const description = document.querySelector("#desc") as HTMLSelectElement;
const people = document.querySelector("#people") as HTMLSelectElement;
const addbtn = document.querySelector("#addbtn") as HTMLSelectElement;

let addData = () => {
  let a: string = title.value;
  let b: string = description.value;
  let c: number = +people.value;

  if (a !== "" && b !== "" && c !== 0) {
    let data: { title: string; description: string; people: number } = {
      title: a,
      description: b,
      people: c,
    };
    addProject(data);
    console.log(data);
  } else {
    alert("Please enter all details");
  }
};

let addProject = (obj: {
  title: string;
  description: string;
  people: number;
}) => {
  let add = document.getElementById("ap") as HTMLUListElement;
  add.innerHTML += `<li class="data" id="dataId" draggable="true" ondragstart="drag(event)">title -> ${obj.title} Description -> ${obj.description} People -> ${obj.people}</li>`;
  let newobj = obj;
  title.value = '';
  description.value = '';
  people.value = '';
  
};

let allowDrop = (ev: any) => {
    ev.preventDefault();
  };

  let drag = (ev: any) => {
    ev.dataTransfer.setData("text/number", ev.target.id);
    console.log("hi")
  };

  let drop = (ev: any) => {
    ev.preventDefault();
    let data = ev.dataTransfer.getData("text/number");
    ev.target.appendChild(document.getElementById(data));
  };
