"use strict";
//import {addProject} from './addproject.js';
const title = document.querySelector("#title");
const description = document.querySelector("#desc");
const people = document.querySelector("#people");
const addbtn = document.querySelector("#addbtn");
let addData = () => {
    let a = title.value;
    let b = description.value;
    let c = +people.value;
    if (a !== "" && b !== "" && c !== 0) {
        let data = {
            title: a,
            description: b,
            people: c,
        };
        addProject(data);
        console.log(data);
    }
    else {
        alert("Please enter all details");
    }
};
let addProject = (obj) => {
    let add = document.getElementById("ap");
    add.innerHTML += `<li class="data" id="dataId" draggable="true" ondragstart="drag(event)">title -> ${obj.title} Description -> ${obj.description} People -> ${obj.people}</li>`;
    let newobj = obj;
    title.value = '';
    description.value = '';
    people.value = '';
};
let allowDrop = (ev) => {
    ev.preventDefault();
};
let drag = (ev) => {
    ev.dataTransfer.setData("text/number", ev.target.id);
    console.log("hi");
};
let drop = (ev) => {
    ev.preventDefault();
    let data = ev.dataTransfer.getData("text/number");
    ev.target.appendChild(document.getElementById(data));
};
