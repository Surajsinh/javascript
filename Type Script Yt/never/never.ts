let genrateError = (massage: string, code: number): never => {
  //never is used for "never return anything"
  throw { massage: massage, errorCode: code };
};
genrateError("error", 404);
