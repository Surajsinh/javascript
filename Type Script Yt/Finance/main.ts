import { Invoice } from "./invoice.js";
import { Payment } from "./Payment.js";
import { HasFormatters } from "./hasFormatter.js";
import { ListTemplate } from "./listTemplate.js";

// inputs
const type = document.querySelector("#type") as HTMLSelectElement;
const toFrom = document.querySelector("#tofrom") as HTMLSelectElement;
const details = document.querySelector("#details") as HTMLSelectElement;
const amount = document.querySelector("#amount") as HTMLSelectElement;

//list template instance

const ul = document.querySelector('ul')!;
const list = new ListTemplate(ul);

//form
const form = document.querySelector(".new-item-form") as HTMLFormElement;
form.addEventListener("submit", (e: Event) => {
  e.preventDefault();

  let doc: HasFormatters;
  if (type.value === "invoice") {
    doc = new Invoice(toFrom.value, details.value, +amount.value);
  } else {
    doc = new Payment(toFrom.value, details.value, +amount.value);
  }

  list.render(doc,type.value,'end');

});

//3624;
