import { HasFormatters } from "./hasFormatter.js";
//classes

export class Invoice implements HasFormatters {
  constructor(
    readonly client: string,
    private details: string,
    public amount: number
  ) {}

  format() {
    return `${this.client} owes Rs.${this.amount} for ${this.details}`;
  }
}
