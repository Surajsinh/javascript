import { Invoice } from "./invoice.js";
import { Payment } from "./Payment.js";
import { ListTemplate } from "./listTemplate.js";
// inputs
const type = document.querySelector("#type");
const toFrom = document.querySelector("#tofrom");
const details = document.querySelector("#details");
const amount = document.querySelector("#amount");
//list template instance
const ul = document.querySelector('ul');
const list = new ListTemplate(ul);
//form
const form = document.querySelector(".new-item-form");
form.addEventListener("submit", (e) => {
    e.preventDefault();
    let doc;
    if (type.value === "invoice") {
        doc = new Invoice(toFrom.value, details.value, +amount.value);
    }
    else {
        doc = new Payment(toFrom.value, details.value, +amount.value);
    }
    list.render(doc, type.value, 'end');
});
//3624;
