import { HasFormatters } from "./hasFormatter.js";
//classes

export class Payment implements HasFormatters {
  constructor(
    readonly recipient: string,
    private details: string,
    public amount: number
  ) {}

  format() {
    return `${this.recipient} is owed Rs.${this.amount} for ${this.details}`;
  }
}
