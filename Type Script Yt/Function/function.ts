let add = (n1: number, n2: number) => {
  return n1 + n2;
};

let printResult = (num: number): void => {
  //you can give return type like number, string ,void ,etc. to function
  console.log("Result :" + num);
};

printResult(add(5, 12));

//let name: undefined;

let comb: Function;
comb = add;  
//comb = 1; will givw an error

console.log(comb(8,8));
