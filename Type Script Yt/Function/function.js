var add = function (n1, n2) {
    return n1 + n2;
};
var printResult = function (num) {
    //you can give return type like number, string ,void ,etc. to function
    console.log("Result :" + num);
};
printResult(add(5, 12));
//let name: undefined;
var comb;
comb = add;
//comb = 1; will givw an error
console.log(comb(8, 8));
