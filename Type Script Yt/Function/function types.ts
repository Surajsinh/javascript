let add = (n1: number, n2: number) => {
  return n1 + n2;
};

let printResult = (num: number): void => {
  console.log("Result :" + num);
};

printResult(add(5, 12));

let comb: (a: number, b: number) => number;  //defining function type with  (perameter)=>return type
comb = add;

//comb = printResult; it will give error

console.log(comb(8, 8));
