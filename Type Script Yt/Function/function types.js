var add = function (n1, n2) {
    return n1 + n2;
};
var printResult = function (num) {
    console.log("Result :" + num);
};
printResult(add(5, 12));
var comb; //defining function type with  (perameter)=>return type
comb = add;
//comb = printResult; it will give error
console.log(comb(8, 8));
