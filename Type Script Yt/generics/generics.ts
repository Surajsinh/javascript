/*NORMAL*/
// const addUID = (obj:object) => {
//     let uid = Math.floor(Math.random()*1000);
//     return{...obj,uid};

// }
// let docObe = addUID({name:'yogi',age:40});

// console.log(docObe);

/*GENERICS*/
//<T>
//<T  extends object>
//<T extends {name:string,age:number}>

const add = <T extends { num1: number; num2: number }>(obj: T) => {
  return obj.num1 + obj.num2;
};
let docObe = add({ num1: 21, num2: 20 });

console.log(docObe);