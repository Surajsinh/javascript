interface Numbers<T> {
  num1: number;
  num2: T;
}

const newDoc: Numbers<number> = {
  num1: 40,
  num2: 30,
};
const newDoc2: Numbers<string> = {
  num1: 40,
  num2: "30",
};

console.log(newDoc,newDoc2);

