//Call a function that return another function 

/*
Approach:

    First call the first function-1.
    Define a function-2 inside the function-1.
    Return the call to the function-2 from the function-1. 
*/

function sum(){
    function sum2(a,b,c){
        return a+b+c;
    }
    return sum2(5,6,7);
}

console.log(sum())