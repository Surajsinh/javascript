/*
Destructing 
*/

//Assign variable from object .

var voxel ={x:3.6,y:7.4,z:6.54};
/*var x = voxel.x;
var y = voxel.y;
var z = voxel.z;*/

const { x , y ,z } = voxel;
console.log(x);
const {x:a,y:b,z:c} = voxel; //asign voxel.x to a
console.log(b);   

//Assign variable from nested object

const nest = {
    start : {x:5,y:6},
    end : {x:6,y:9}
};
const {start : {x:startX,y:startY}} = nest;
console.log(startX);

//Assign variable from Array

const [q,r] = [1,2,3,4,5];
console.log(q,r);   //1,2
const [,l,,,m] = [1,2,3,4,5];
console.log(l,m);   //2,5


//Rest oprater to reassign Array Elements.

const [ab,cd,...arg] = [1,2,3,4,5];
console.log(ab,cd);   //1,2  [3,4,5]
console.log(arg);

//pass an object as a Function's Parameters

const profileUpdate = (profileData) => {
    const { name,age,nationality,location } = profileData;
       
}