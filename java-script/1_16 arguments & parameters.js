// arguments & parameters 

const  speak = function(name="hear" , time="night") {
    console.log(`Good ${time} ${name}`);    
};

speak();
speak("veer" , "morning");

console.log("-----------------------------------------------");


const calcArea = function (radius) {
    let area = 3.14 * radius**2;
    console.log(area);
}

calcArea(5);
calcArea(10);