// "use strict";  //code after use strict will follwe strict roules of js
// /* strict mode */

// myFunction = () => {
//     "use strict";
//     y = 3.14; //you can not define without var or let in strict mode

// }

// //converts mistakes into errors

// var x = 3.14;
// delete x;

// var obj = {};
// Object.defineProperty(obj,"x",{value:0,writable:false}); //you can not write value without write permition in strict
// obj.x = 3.14;


// var obj = {get x() {return 0}};  //you can not asign new value in get in strict mode
// obj.x = 3.14;

// delete Object.prototype; //you can not delete in strict mode.


// sum = (a,a,c) => {
//     'use strict';
//     return a+b+c;          //will give error in strict mode becouse of defrient perameters and value in use
// }

// //evel 

// var x = 17;
// var evelX = eval("'use strict';var x = 42; x;");
// console.assert(x == 17);
// console.assert(evelX == 42);
// console.log(x==17)
// console.log(evelX==42)


// a = 10;
// console.log(a);
// abc = () => {
//     "use strict";
//     b = 10;
//     console.log(b); 
// }
// abc(); //will give error


function sum(a , b , b){
    return a+b+b;
}

console.log(sum(10,20,50));

'use strict';
function sum2(e , f , f){
    'use strict';
    return e+f+f;
}

console.log(sum(10,20,50)); //will give error

