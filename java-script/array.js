// Array

// literal constructor
//[element0, element1, /* ... ,*/ elementN]

// let fruits = ['Apple', 'Banana'];
// console.log(fruits.length); // 2
// console.log(fruits[0]);     // "Apple"

// Array constructor with a single parameter
//Arrays can be created using a constructor with a single number parameter. An array with its length property set to that number and the array elements are empty slots. 

// let fruit = new Array(2);
// console.log(fruit.length);
// console.log(fruit[0]);


// Array constructor with multiple parameters

let fruits = new Array('Apple','Banana');
console.log(fruits.length);
console.log(fruits[0]);

