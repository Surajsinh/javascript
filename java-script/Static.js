// The static keyword defines a static method or property for a class,or a class static initialization block .
// Neither static methods nor static properties can be called on instances of the class.
// Instead, they're called on the class itself. 

class classWithStaticMethod{
    static staticProperty = 'someValue';
    static staticMethod (){
        return 'Static Method is Running....';
    }
    static{
        console.log('Class static initalization block called');
    }
}

let a = new classWithStaticMethod;
console.log(a.staticMethod());

console.log(classWithStaticMethod.staticMethod());
console.log(classWithStaticMethod.staticProperty);