//search in an array 
let array = ['a','b','c','d','e','f','g' , 40];

//indexOf   
console.log(array.indexOf('a'));

//lastindexOf search array from right to left
console.log(array.lastIndexOf('a'));

// (-1 mening value not found)
console.log(array.lastIndexOf(10));

let ar = array.findIndex((value,index,arr)=>{
    console.log(`value : ${value} | index : ${index} | Array : ${arr}`);
    return value > 30;
});

console.log(ar);


