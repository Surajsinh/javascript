
let pokemon = {
    firstname: 'Pika',
    lastname: 'Chu ',
    getPokeName: function() {
        var fullname = this.firstname + ' ' + this.lastname;
        return fullname;
    }
};

let pokemonName = function() {
    console.log(this.getPokeName() + 'I choose you!');
};

let logPokemon = pokemonName.bind(pokemon); // creates new object and binds pokemon. 'this' of pokemon === pokemon now
console.log(logPokemon);
logPokemon(); // 'Pika Chu I choose you!'



// call (function borowing) : we can use function  from other objects and use in other object.

let getPokeName = function() {
    var fullname = this.firstname + ' ' + this.lastname;
    return fullname;
}

let pok = {
    firstname : "charli",
    lastname : "zard"
}

let cz = getPokeName.call(pok);
console.log(cz);

// //pass perameters in call function
// let getPokeName = function(type) {
//     var fullname = this.firstname + ' ' + this.lastname + ' Type : ' + type;
//     return fullname;
// }

// let pok = {
//     firstname : "charli",
//     lastname : "zard"
// }

// let cz = getPokeName.call(pok,'Fire');
// console.log(cz);

// apply() we can pass perameters with Array with apply()

let getCzPokeName = function(type,type2) {
         var fullname = this.firstname + ' ' + this.lastname + ' Type : ' + type + ' Type 2 : ' + type2 ;
         return fullname;
     }

let cza = getCzPokeName.apply(pok,["fire","wind"]);
console.log(cza);

// let newPokeName = getCzPokeName.bind(pok,"Fire","Wind");
// console.log(newPokeName); //Bind Returns new Function

// console.log(newPokeName());