/* spread opreter and rest opreter*/


//add element of exsiting array into new array

var certsToAdd = [' Algorithms and Data Structures ' , ' Front End Libraries '];
var certifications = [ ' Responsive Web Design ' , ...certsToAdd , 'Data Visualization' , ' Api and Microservice ' , 'Quality Assurance'];
console.log(certifications);

//pass element of an array as argument to a function

function addNumber(x,y,z){
    console.log(x+y+z);
}
var args = [0,1,2];
addNumber(...args);

//copy array

var arr = [1,2,3];
var arr2 = [...arr];
arr2.push(4);
console.log(arr);
console.log(arr2);

//concatenate array

console.log(arr.concat(arr2));

var arr3 = [...arr,...arr2];
console.log(arr3);

//Rest:condense multiple element into an array

let multiply = (multiplier , ...theArgs) => {
    return theArgs.map((element)=>{
        return multiplier * element; 
    });
}

let arr0 = multiply(2,1,2,3);
console.log(arr0);