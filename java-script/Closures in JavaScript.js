//JavaScript Closures

/*
JavaScript variables can belong to the local or global scope.
Global variables can be made local (private) with closures.
 */

//What is closure
//A closure is an inner function that has access to outer (enclosing) function's variable

/*
Every closure has three scopes:
1>local
2>outer function
3>Global
*/

var i = 10;
function show(){
    var j = 20;  // j is local variable of outer function.
    console.log(j);
    function inFun(){
        var k = 30 ; // k is local variable of inner function. 
        console.log(k);
        console.log(j);
        console.log(i);
    }
    inFun();
}

show();