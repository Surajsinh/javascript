//Error Handling
//try , catch , finally ,throw

/*
The try statement defines a code block to run (to try).

The catch statement defines a code block to handle any error.

The finally statement defines a code block to run regardless of the result.

The throw statement defines a custom error.
*/

// try {
//     Block of code to try
// }
//   catch(err) {
//     Block of code to handle errors
// }

try{
    console.log("start of try");
    unicycle; //error
    console.log("end of try");
}catch(err){
    console.log("Error has accured:" + err)
}
finally{
    console.log("this will alwas run");
}


//throw

let json = '{"age":30}';
try{
    let user = JSON.parse(json);
    if(!user.name){
        throw new SyntaxError("Incomplate Data : no name");
    }
    console.log(user.name);
}catch(e){
    console.log("JSON Error : " + e.message);
}
