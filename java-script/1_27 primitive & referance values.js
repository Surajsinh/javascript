//primitive values

let scoreOne = 50;
let scoreTwo = scoreOne;

console.log(`scoreone : ${scoreOne} scoretwo : ${scoreTwo}` );


scoreOne = 100;
console.log(`scoreone : ${scoreOne} scoretwo : ${scoreTwo}` );

//reference values
 

const userOne = {name:'ryu',age:30};
const userTwo = userOne;

console.log(`Userone name is ${userOne.name} and he/she is ${userOne.age} years old . \nUsertwo name is ${userOne.name} and he/she is ${userOne.age} years old .`);

userOne.name = 'loy';
console.log(`Userone name is ${userOne.name} and he/she is ${userOne.age} years old . \nUsertwo name is ${userOne.name} and he/she is ${userOne.age} years old .`);

userTwo.age = 40 ;
console.log(`Userone name is ${userOne.name} and he/she is ${userOne.age} years old . \nUsertwo name is ${userOne.name} and he/she is ${userOne.age} years old .`);
