//while loop

// for(let i = 0  ; i < 5 ; i++){
//     console.log("hello " + i)
// }

let i = 0
while(i < 10){
    console.log("hello "+i)
    i++
}

console.log('----------finish---------')

const names = ['anil' , 'madhu' , 'manish' , 'chirag'];
let j = 0;
while(j < names.length){
    console.log(names[j])
    j++
}

console.log('----------finish---------')
