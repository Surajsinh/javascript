//Array Iteration or Array traversal
var array = [10,20,32,47,84,2];

array.forEach((value) => {                
    console.log(value);
});

console.log("-----------------------------------------");

arrayIteration = (value,index,arr) => {       //(value,index,array) first preamter hold value second index and third array in 
    console.log(value);
}

array.forEach(arrayIteration);

console.log("-----------------------------------------");

//for loop
for(var i = 0 ; i< array.length ; i++)
{
    console.log(`index ${i} = ${array[i]}`);
}

console.log("-----------------------------------------");

//concatonate

var txt = '';
arrayIteration1 = (value,index,arr) => {       //(value,index,array) first preamter hold value second index and third array in 
    txt += value+'\n';
    console.log(txt);
}

array.forEach(arrayIteration1);

console.log("-----------------------------------------");

//map()

let arAAy = array.map((value,index,arr)=>{
    return value;       //returns every value an array
});
let arAAy2 = array.map((value,index,arr)=>{
    return value+10;
});

console.log(arAAy);
console.log(arAAy2);

console.log("-----------------------------------------");

//filter()

let filt = array.filter((value,index,arr)=>{
    return value > 20; //return as array
});

console.log(filt);

console.log("-----------------------------------------");

//reduce() it will do left to right itration

let sum = array.reduce((sum,value,index,arr)=>{
    return sum + value; //you can also use  -,*,/,%
});
console.log(sum);

console.log("-----------------------------------------");

//reduceRight() it will do right to left itration

let sum1 = array.reduceRight((sum,value,index,arr)=>{
    return sum + value; //you can also use  -,*,/,%
});
console.log(sum1);


console.log("-----------------------------------------");

//every() all array value pass condition

var below = array.every((value,index,arr)=>{
    return value > 20;
}); 
console.log(below);  //false

var below1 = array.every((value,index,arr)=>{
    return value > 1;
}); 
console.log(below1);  //true

console.log("-----------------------------------------");

//some()

var some1 = array.some((value,index,arr)=>{
    return value > 20;
}); 
console.log(some1);  //true

var some2 = array.some((value,index,arr)=>{
    return value < 0;
}); 
console.log(some2);  //false

console.log("-----------------------------------------");



//  //Iterating Over a String

//const Name = "Hello";

// // List all Elements
// let text = ""
// for (const x of Name) {
//   text += x + "\n";
// }
// console.log(text);

//Iterating Over an Array

// // Create a Array
// const numbers = [1,2,3,4,5];

// // List all Elements
// let no = "";
// for (const x of numbers) {
//   no += x + "\n";
// }
// console.log(no);


//Iterator

// function fruitsIterator(values){
//     let count = 0;
//     //we will return an object
//     return{
//         hi : () => {
//             if(count < values.length){
//             //we will return below objects
//             return {
//                 value : values[count++],
//                 done : false
//                }   
//         }
//         else{
//             //we will return below object with only done
//             return {
//                 done : true
//            }
//         }
//         }
    
//     }
// }

// const myArray = ['Apple',"Grapes",'Orange',"Banana"];
// console.log("My array is ",myArray);

// //Using  the iterator
// const fruits =  fruitsIterator(myArray);
// console.log(fruits.hi());
// console.log(fruits.hi());
// console.log(fruits.hi());
// console.log(fruits.hi());
// console.log(fruits.hi());




// //symbol.iterator 
// const iterable1 = {};

// iterable1[Symbol.iterator] = function* () {
//   yield 1;
//   yield 2;
//   yield 3;
// };

// console.log([...iterable1]);

// Symbol.iterator

// let range = {
//     from : 2,
//     to :7
// };

// range[Symbol.iterator] = function(){
//     return {
//         now: this.from,
//         end: this.to,
//         next(){
//             if(this.now <= this.end){
//                 return { done:false,value: this.now++};
//             }else{
//                 return {done: true};
//             }
//         }
//     }
// };

// for (let i of range){
//     console.log(i);
// }

