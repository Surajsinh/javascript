let country = 'India';
let hh = 0;
let mm = 0;
let ss = 0;

let getName = (id) => {
    country = id;
}

let getCountry = () => {
    switch (country) {
        case "India":
            let ti = new Date().toLocaleString("en-US", { timeZone: 'Asia/Kolkata',timeStyle : 'medium', hourCycle: 'h24' });
            document.getElementById("time").innerHTML ="India time = "+  ti;
            hh = ti.substring(0,2);
            mm = ti.substring(3,5);
            ss = ti.substring(6,8);
            hrotation = 30 * +hh + mm / 2;
            mrotation = 6 * +mm;
            srotation = 6 * +ss;
            hours.style.transform = `rotate(${hrotation}deg)`;
            minutes.style.transform = `rotate(${mrotation}deg)`;
            seconds.style.transform = `rotate(${srotation}deg)`;
            break;

        case "losAngeles":
            let tl = new Date().toLocaleString("en-US", { timeZone: 'America/Los_Angeles',timeStyle: 'medium', hourCycle: 'h24' })
   	        document.getElementById("time").innerHTML = "LosAngeles time = "+ tl; 
            hh = tl.substring(0,2);
            mm = tl.substring(3,5);
            ss = tl.substring(6,8);
            hrotation = 30 * +hh + mm / 2;
            mrotation = 6 * +mm;
            srotation = 6 * +ss;
            hours.style.transform = `rotate(${hrotation}deg)`;
            minutes.style.transform = `rotate(${mrotation}deg)`;
            seconds.style.transform = `rotate(${srotation}deg)`;
            break;

        case "New York":
            let tn = new Date().toLocaleString("en-US", { timeZone: 'America/New_York',timeStyle: 'medium', hourCycle: 'h24' })
    	    document.getElementById("time").innerHTML ="New York time = "+ tn;
            console.log(tn.substring(0,2));
            hh = tn.substring(0,2);
            mm = tn.substring(3,5);
            ss = tn.substring(6,8); hrotation = 30 * +hh + mm / 2;
            mrotation = 6 * +mm;
            srotation = 6 * +ss;
            hours.style.transform = `rotate(${hrotation}deg)`;
            minutes.style.transform = `rotate(${mrotation}deg)`;
            seconds.style.transform = `rotate(${srotation}deg)`;
            break;

        case "Hong_Kong":
            let th =new Date().toLocaleString("en-US", { timeZone: 'Asia/Hong_Kong', timeStyle: 'medium', hourCycle: 'h24' })
    	    document.getElementById("time").innerHTML ="Hong_Kong time = "+ th ;
            hh = th.substring(0,2);
            mm = th.substring(3,5);
            ss = th.substring(6,8);hrotation = 30 * +hh + mm / 2;
            mrotation = 6 * +mm;
            srotation = 6 * +ss;
            hours.style.transform = `rotate(${hrotation}deg)`;
            minutes.style.transform = `rotate(${mrotation}deg)`;
            seconds.style.transform = `rotate(${srotation}deg)`;
            break;

        case "karachi":
            let tk = new Date().toLocaleString("en-US", { timeZone: 'Asia/Karachi', timeStyle: 'medium', hourCycle: 'h12' });
	        document.getElementById("time").innerHTML ="Karachi time = "+ tk ;
            hh = tk.substring(0,2);
            mm = tk.substring(3,5);
            ss = tk.substring(6,8);hrotation = 30 * +hh + mm / 2;
            mrotation = 6 * +mm;
            srotation = 6 * +ss;
            hours.style.transform = `rotate(${hrotation}deg)`;
            minutes.style.transform = `rotate(${mrotation}deg)`;
            seconds.style.transform = `rotate(${srotation}deg)`;
            break;
    }
}
setInterval(getCountry, 1000);