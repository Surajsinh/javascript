//Creating Date Objects

// Date objects are created with the new Date() constructor.
// There are 4 ways to create a new date object:

// new Date()
// new Date(year, month, day, hours, minutes, seconds, milliseconds)
// new Date(milliseconds)
// new Date(date string)

const dt = new Date(2018, 11, 24, 9, 33, 30, 100);
console.log(dt); 

// JavaScript Date Input
// There are generally 3 types of JavaScript date input formats:

// ISO Date 	"2015-03-25" (The International Standard)
// Short Date 	"03/25/2015"
// Long Date 	"Mar 25 2015" or "25 Mar 2015"

let d =  new Date("2015-03-25"); //yy-mm-dd
console.log(d);
d = new Date("2015-03"); //yy-mm
console.log(d);
d = new Date("2015"); //yy
console.log(d);
// Date and time is separated with a capital T.
// UTC time is defined with a capital letter Z.
d = new Date("2015-03-25T12:00:00Z"); //yy-mm-dd time
console.log(d);
d = new Date("03/22/2015"); //mm-dd-yy
console.log(d);
d = new Date("25 Mar 2015"); 
console.log(d);
d = new Date("January 25 2015"); 
console.log(d);

//Get date Method

// Method 	            ->       Description
// getDate() 	        ->       Get the day as a number (1-31)
// getDay() 	        ->       Get the weekday a number (0-6)
// getFullYear() 	    ->       Get the four digit year (yyyy)
// getHours() 	        ->       Get the hour (0-23)
// getMilliseconds() 	->       Get the milliseconds (0-999)
// getMinutes() 	    ->       Get the minutes (0-59)
// getMonth() 	        ->       Get the month (0-11)
// getSeconds() 	    ->       Get the seconds (0-59)
// getTime() 	        ->       Get the time (milliseconds since January 1, 1970)

let a = new Date();
console.log(`Today's Date is =>${a.getDate()}`);
console.log(`Day =>${a.getDay()}`);
console.log(`FullYear => ${a.getFullYear()}`);
console.log(`Hours => ${a.getHours()}`);
console.log(`Millisecond => ${a.getMilliseconds()}`);
console.log(`Minuts => ${a.getMinutes()}`);
console.log(`Soconds => ${a.getSeconds()}`);
console.log(`Time =>${a.getTime()}`);


//Set Date Method

// setDate() 	        ->      Set the day as a number (1-31)
// setFullYear() 	    ->      Set the year (optionally month and day)
// setMilliseconds() 	->      Set the milliseconds (0-999)
// setHours() 	        ->      Set the hour (0-23)
// setMinutes() 	    ->      Set the minutes (0-59)
// setMonth() 	        ->      Set the month (0-11)
// setSeconds() 	    ->      Set the seconds (0-59)
// setTime() 	        ->      Set the time (milliseconds since January 1, 1970)
console.log(`\n`);


a.setDate(15);
console.log(`Date => ${a.getDate()}`);
a.setDate(a.getDate()+50); //The setDate() method can also be used to add days to a date
console.log(`Date + 50 Days => ${a.getDate()}`);
a.setFullYear(2025);
console.log(`FullYear => ${a.getFullYear()}`);
a.setFullYear(2021,11,3);
console.log(`FullYear => ${a.getFullYear()}`);
a.setMinutes(30);
console.log(`Minutes => ${a.getMinutes()}`);
a.setHours(22);
console.log(`Hours => ${a.getHours()}`);
a.setMonth(11);
console.log(`Month => ${a.getMonth()}`);
a.setSeconds(30);
console.log(`Second => ${a.getSeconds()}`);