class Rectangle {
    //constructor setup object
    constructor(width,length,color){
        console.log(`Rectangle is being created. `);
        // this.width = 10;
        // this.length = 20;
        // this.color =  'blue';
        this.width = width;
        this.length = length;
        this.color =  color;
    }

    //Instunc method
    getArea() {
        return this.width*this.length;
    }
    printDiscription(){
        console.log(`I am a Rectangle of widhth:${this.width} and length ${this.length}.`)
    }
}

let myRectangle = new Rectangle();
let myRectangle1 = new Rectangle(12,24,'black');
console.log(myRectangle);
console.log(myRectangle1);
console.log(`Rectangle Area is = ${myRectangle1.getArea()}`);
console.log(myRectangle1.printDiscription());


class Square {
    constructor(width){
        this.width = width;
        this.numOfRequest = 0;
    }
    //get & set
    get area (){
        this.numOfRequest++;
        return this.width**2;
    }
    set area(area){
        this.width = Math.sqrt(area);
    }

}
//get
let Square1 = new Square(25);
console.log(`Area of Square is :- ${Square1.area}`);

let Square2 = new Square(4);
console.log(`Area of Square is :- ${Square2.area}`);


//set
Square1.area=9;
console.log(Square1.width);
console.log(`Area of Square is :- ${Square1.area}`);

console.log(`Number Of ${Square1.numOfRequest}`);

//static
class Sqr {
    constructor(width){
        this.width = width;
    }
    static equals (a,b){
        return a.width**2 == b.width**2;
    }
    static isValid(width,height){
        return width === height;

    }
}
let sqr1 = new Sqr(8);
let sqr2 = new Sqr(8);
console.log(Sqr.equals(sqr1,sqr2));

console.log(Sqr.isValid(4,4));

//parent and child class

class person {
    constructor (name,age){
        this.name = name;
        this.age = age;
    }
    describe(){
        console.log(`I am ${this.name} and I am ${this.age} Years old. `);
    }
}
class Programmer extends person {
    constructor (name,age,exp){
        super(name,age);
        this.exp = exp;
    }    
    code(){
        console.log(`${this.name} is coding..`);
    }
}

// let per1 = new person('jeff',45);
// let pro1 = new programmer('don',37,5);

let programmers = [
    new Programmer('done',53,10),
    new Programmer('nik',47,7),
    new Programmer('fin',72,30)
]

// console.log(per1);
// console.log(pro1);

//pro1.code();

function developSoftware (programmers){
    //Devlop software
    for (let programmer of programmers){
        programmer.code();
    }
}

developSoftware(programmers);


//Class Polymorphism

class Animal {
 constructor(name){
    this.name = name;
 }
 makeSound(){
     console.log("Generic Animal Sound !");
 }   
}

class Dog extends Animal{
    constructor(name){
        super(name);
    }
    makeSound(){
        super.makeSound();
        console.log('wof! wof!');
    }
}

const a1 = new Animal("Dog");
const a2 = new Dog(`jef`);
a1.makeSound();
a2.makeSound();