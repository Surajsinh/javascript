//Add, Remove, Insert, Replace Elements in Array


let array = ['hi','micro','thor','loki','black'];

//array.length

console.log(array.length);

//array add

array.push('caption');
console.log(array);

//array remove

array.pop('caption');
console.log(array);

//array splice

array.splice(1,1);    //(index,n(remove next n value after given index));
console.log(array);

array.splice(2,0,'barry'); //wii add 'barry' at second index
console.log(array);