//sort an array 
const names = ['File','Folder','Align','Center'];
names.sort();
console.log(names);

const no = [13,4,70,37,35,07,6];
no.sort(); //output = 13, 35, 37, 4,6,  7, 70
console.log(no);

compare = (a,b) => {
    /*
    1)   < 0      --a will come first
    2)   0        --Nothing will change
    3)   > 0      --b will come first
     */

    return a-b;
}

no.sort(compare);
console.log(no);

//array of object
const product = [
    {
        name : 'laptop',
        price : 1000
    },
    {
        name:'mouse',
        price: 40
    },
    {
        names:'keyboard',
        price:50
    }
];

product.sort((a,b)=>{
    return a.price - b.price;
});

console.log(product);
