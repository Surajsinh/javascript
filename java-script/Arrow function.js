//normal function
let greet = function(){
    console.log("hello worlds");
    return 1;
}
greet();


//arrow function

/* 
Arrow functions were introduced in ES6.
Arrow functions allow us to write shorter function syntax
 */

let greet1 = () => console.log("hello univerce.");
//This works only if the function has only one statement.
greet1();

//---------------------------------

sum = (a,b) => {
    return a+b;
}
console.log(sum(4,5));

//----------------------------------

let myFunction = (a, b) => a * b;
//This works only if the function has only one statement.
console.log(myFunction(3,5));

