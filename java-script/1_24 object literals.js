// // object literals

// A JavaScript object literal is a comma-separated list of name-value pairs wrapped in curly braces. 
// Object literals encapsulate data, enclosing it in a tidy package. 
// This minimizes the use of global variables which can cause problems when combining code.

// let user = {
//     name : 'crystal',
//     age : 30,
//     email : 'sun@mail.com'
// };

// console.log(user);
// console.log(user.name);
// user.age = 45;
// console.log(user);

// console.log(typeof user);

let myObject = {
    sProp : 'some string value',
    numProp : 2,
    bProp : false
};

//Object literal property values can be of any data type, including array literals, functions, and nested object literals.

let swep = {
    //an array literal
    images : ["smile.gif","avenger.jpeg","crown.png"],
    pos: {
        //nested object literal
        x:40,
        y:30
    },
    onSwap: function(){
        return console.log("onSwap function is runnning......");
    }
};


swep.onSwap();
