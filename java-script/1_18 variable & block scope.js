//variable & block scope || global local

let age = 30; //globle variable

if (true){
    let age2 = 40; //local variable 

    console.log('inside if '+ age);
    console.log('inside if '+ age2);

}

console.log('outside if '+ age);
console.log('outside if '+ age2); //will give error !