//object literals

let user = {
    name : 'crystal',
    age : 30,
    email : 'sun@mail.com',
    blogs : ['why mac & chess rules','10 things to make'],
    login : function(){
        console.log('the user logged in');
    },
    logout : function(){
        console.log('the user logged out');
    },
    logblogs : function(){
        console.log(this.blogs);
        this.blogs.forEach(blog =>{
            console.log(blog);
        });
    },
};


user.login();
user.logout();
user.logblogs();

